<?php

namespace App\Http\Controllers;

use App\asignarpaginas;
use App\impresiones_usuario;
use App\permiso_impresion;
use Illuminate\Http\Request;

class copController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $asignarpaginas = asignarpaginas::all();
        $impre_usuario = impresiones_usuario::all();
        $permi_usuario = permiso_impresion::all();
        return view('home', compact('asignarpaginas','impre_usuario','permi_usuario'));
        
    }

    public function historico()
    {
        return view('historico');
        
    }
     
    public function multifuncionales()
    {
        $ruta_powershell = 'powershell.exe'; #Necesitamos el powershell
        $opciones_para_ejecutar_comando = "-c";#Ejecutamos el powershell y necesitamos el "-c" para decirle que ejecutaremos un comando
            $espacio = " "; #ayudante para concatenar
            $comillas = '"'; #ayudante para concatenar
            $comando = 'get-WmiObject -class Win32_printer |ft name'; #Comando de powershell para obtener lista de impresoras
            $lista_de_impresoras = array(); #Aquí pondremos las impresoras
            exec(
                $ruta_powershell
                . $espacio
                . $opciones_para_ejecutar_comando
                . $espacio
                . $comillas
                . $comando
                . $comillas,
                $resultado,
                $codigo_salida);

            if ($codigo_salida === 0) {
                if (is_array($resultado)) {
                    #Omitir los primeros 3 datos del arreglo, pues son el encabezado
                    for($x = 3; $x < count($resultado); $x++){
                        $impresora = trim($resultado[$x]);
                        if (strlen($impresora) > 0) # Ignorar los espacios en blanco o líneas vacías
                            array_push($lista_de_impresoras, $impresora);
                    }
                }
                //echo "<pre>";
                //print_r($lista_de_impresoras);
                //echo "</pre>";
            } else {
                //echo "Error al ejecutar el comando.";
            }



        return view('multi', compact('lista_de_impresoras'));
        
    }

    public function _exec($cmd){
        shell_exec('SCHTASKS /F /Create /TN _proc /TR "' . $cmd . '"" /SC DAILY /RU INTERACTIVE');
        shell_exec('SCHTASKS /RUN /TN "_proc"');
        shell_exec('SCHTASKS /DELETE /TN "_proc" /F');
    }

    public function _cron($cmd, $tie, $titulo)
    {
        shell_exec('SCHTASKS /F /Create /mo '.$tie.'  /TN _'.$titulo.' /TR "'.$cmd.'"" /SC MINUTE /RU INTERACTIVE');
        shell_exec('SCHTASKS /RUN /TN "_'.$titulo.'"');
    }
    
    public function activarcron()
    {
        

        $event = "C:/xampp/htdocs/public/deventid.exe";
        $update = "C:/xampp/htdocs/public/update.exe";
        $desactivar_user = "C:/xampp/htdocs/public/desactivar_user.exe";
        $force = "C:/xampp/htdocs/public/force.exe";

        
        self::_cron($event, "5", "devento");
        self::_cron($update, "10", "update");
        self::_cron($desactivar_user, "20", "desactivar_user");
        self::_cron($event, "65", "force");
        
        return redirect('/home');
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
