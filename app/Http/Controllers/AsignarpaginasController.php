<?php

namespace App\Http\Controllers;

use App\asignarpaginas;
use App\permiso_impresion;
use App\impresiones_usuario;
use Illuminate\Http\Request;
use DB;

class AsignarpaginasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $asignar = asignarpaginas::all();
        return view('asignarpaginas.index', compact('asignar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\asignarpaginas  $asignarpaginas
     * @return \Illuminate\Http\Response
     */
    public function show(asignarpaginas $asignarpaginas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\asignarpaginas  $asignarpaginas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $asignarnumero = asignarpaginas::findOrFail($id);
        return view('asignarpaginas.edit', compact('asignarnumero'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\asignarpaginas  $asignarpaginas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = $request->validate([
            'PagCout'=>'required'
        ]);
                
        $usuario = asignarpaginas::findOrFail($id);
        
        $usuario_impre = impresiones_usuario::findOrFail($id);

        $valorCout = permiso_impresion::findOrFail($id); 

        //  Validaciones de entrada para el control de impresiones 

            if ( $valorCout->Cout != null ) {
                
                if ( $valid['PagCout'] > $valorCout->Cout ) {
                    $result = $valid['PagCout'] + $valorCout->Cout;
                    //$result = ($tmp + $valorCout->Cout);
                }
                else if ( $valid['PagCout'] < $valorCout->Cout ) {
                    $tmp = ( $usuario->PagCout - $valid['PagCout'] );
                    $result = ($valorCout->Cout - $tmp);
                }

                permiso_impresion::where('id', $id)->update(array('Cout' => $result));
            
            }

            asignarpaginas::whereId($id)->update($valid);
            
            if ( $usuario->PagCout == null || $usuario->PagCout == "" ) {
                
                $file = fopen("atemp.ps1", "w");
                $script = '$sama = Get-ADUser -Filter "Name -eq '.$usuario->UserPc.'" | Select-Object samaccountname; Add-ADGroupMember -Identity act_grupo -Members $sama.samaccountname';
                fwrite($file,$script. PHP_EOL);
                fclose($file);
                shell_exec("powershell.exe .\atemp.ps1");

            }


        //$asignarnumero = asignarpaginas::findOrFail($id);
        //return view('asignarpaginas.edit', compact('asignarnumero'));
        $asignar = asignarpaginas::all();
        return view('asignarpaginas.index', compact('asignar'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\asignarpaginas  $asignarpaginas
     * @return \Illuminate\Http\Response
     */
    public function destroy(asignarpaginas $asignarpaginas)
    {
        //
    }
}
