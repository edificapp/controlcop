function CreaEstructura
{
   $NombreOU = $args[0]
   $dominioLDAP = $args[1]
   $rutaOU = ("OU="+$NombreOU+","+$dominioLDAP)
   if ([adsi]::Exists(("LDAP://" + $rutaOU))) 
   {
      write-host ("La Unidad Organizativa " + $NombreOU + " ya existe.") -ForegroundColor Red
   } 
   else
   {
      write-host ("Creando la OU "+$NombreOU+","+$dominioLDAP) -ForegroundColor Green
      new-ADOrganizationalUnit -DisplayName $NombreOU -Name $NombreOU -path $dominioLDAP
   }
}

function NuevoGrupoSeguridad
{
   $NombreGrupo = $Args[0]
   $NombreOU = $args[1]
   #$dominioLDAP = "DC=jmsolanes,DC=local"
   $rutaOU = ("OU="+$NombreOU+","+$dominioLDAP)
   if (Get-ADGroup -Filter {SamAccountName -eq $NombreGrupo})
   {
     write-host ("El grupo de seguridad " + $NombreGrupo + " ya existe.") -ForegroundColor Red
   }
   else
   {
     New-ADGroup -DisplayName $NombreGrupo -Name $NombreGrupo -GroupScope DomainLocal -GroupCategory Security -Path $rutaOU
   }
}


$dominioLDAP="dc=FERGON,dc=SAS"
CreaEstructura "enableprint" $dominioLDAP
CreaEstructura "disableprint" $dominioLDAP

NuevoGrupoSeguridad "act_grupo" "enableprint"
NuevoGrupoSeguridad "iact_grupo" "disableprint"

#New-GPO -Name ActivarImpresoraGPO -Comment "Activar Impresora GPO." 
#New-GPO -Name desactivarImpresoraGPO -Comment "Desactivar Impresora GPO." 

new-gpo -name ActivarImpresoraGPO -Comment "Activar Impresora GPO." | new-gplink -target "ou=enableprint,dc=FERGON,dc=SAS"
new-gpo -name DesactivarImpresoraGPO -Comment "Desactivar Impresora GPO." | new-gplink -target "ou=disableprint,dc=FERGON,dc=SAS"