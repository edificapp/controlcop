Param( [string]$user )
$dn = (Get-ADUser -Identity $user -Properties DistinguishedName).DistinguishedName 
$parent = $dn.Split(',',2)[1] 
$user

Add-ADGroupMember -Identity "iact_grupo" -Members $user
Remove-ADGroupMember -Identity "act_grupo" -Members $user -Confirm:$false

