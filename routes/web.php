<?php

//use Illuminate\Support\Facades\Route;
//use App\impresion_paginas;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

/*Route::get('/activarcron', function () {
    return view('layouts.activarcron');
});*/

Auth::routes();


Route::get('/home', 'copController@index')->name('home');

Route::get('/user', 'UsersController@index')->name('administradores');

Route::get('/historico', 'copController@historico');

Route::get('/multifun', 'copController@multifuncionales');

Route::get('/actcron', 'copController@activarcron');

Route::resource('asignar', 'AsignarpaginasController');
