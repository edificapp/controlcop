        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider"  style="font-size: 17px;">
                                Menu
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5" style="font-size: 17px;"><img class="logo-img" src="{{ asset('assets/images/user.png') }}" alt="logo" width="21" style="margin-right: 7px;"> Usuarios</a>
                                <div id="submenu-5" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/asignar') }}"> Asignar Paginas</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link active" href="{{ url('/multifun') }}" data-toggle=""  style="font-size: 17px;" ><img class="logo-img" src="{{ asset('assets/images/055.png') }}" alt="logo" width="21" style="margin-right: 7px;"></i> Multifuncionales</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ url('/historico') }}" data-toggle=""  style="font-size: 17px;" ><i class="fas fa-fw fa-table" style="font-size: 20px;"></i> Historico</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ url('/user') }}" data-toggle=""  style="font-size: 17px;" ><img class="logo-img" src="{{ asset('assets/images/ad.png') }}" alt="logo" width="21" style="margin-right: 7px;"></i> Administrador</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ url('/actcron') }}" data-toggle="" id="acron"  style="font-size: 17px;"><i class="fas fa-fw fa-clock" style="font-size: 20px;"></i> Activar CRON</a>
                            </li>
                                                        
                        </ul>
                    </div>
                </nav>
            </div>
        </div>



