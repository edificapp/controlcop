@extends('layouts.plantilla')
@section('contenidoGeneral')

	<div class="dashboard-wrapper">
		<div class="container-fluid dashboard-content ">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
					<div class="page-header">
						<h2 class="pageheader-title">Control COP - control de impresiones </h2> </div>
					</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
					<div class="page-header" align="right"> <img class="logo-img" src="{{ asset('assets/images/fergon.png') }}" alt="logo" height="45"> </div>
				</div>
			</div>			
			<div class="row">
                    <!-- ============================================================== -->
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2"></div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                        <div class="card">
                            <div class="card-header">
                                <div style="text-align: center; margin-bottom: 30px;">
                                    <img class="logo-img" src="{{ asset('assets/images/usuario.png') }}" alt="logo" height="75">
                                    <span style="font-size: 23px; text-align: center;color: #898989; font-family: arial;">Asignar paginas</span>
                                </div>
                            </div>
                            <div class="card-body">
                            	@if ($errors->any())
								  <div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
										  <li>{{ $error }}</li>
										@endforeach
									</ul>
								  </div><br />
								@endif
                                <form method="post" action=" {{ route('asignar.update', $asignarnumero->id) }}">
                                @method('PATCH') 
            					@csrf
                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label class="control-label" style="position:relative; top:7px;" title="Nombre de pila o primer nombre">Nombre de pila o primer nombre:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="UserPc" value=" {{ $asignarnumero->UserPc}}"  autocomplete="off" disabled="">
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="control-label" style="position:relative; top:7px;" title="Nombre completo del usuario">Cantidad de paginas:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="PagCout" value="{{ $asignarnumero->PagCout }}"  autocomplete="off" autofocus="" >
                                    </div>
                                       
                                </div>
                            </div>

                            <div class="modal-footer">
                            	<button type="submit" class="btn btn-primary">Actualizar</button>

                            </form>
                                <a href="{{ url('/asignar') }}" class="btn btn-default"><span class="fa fa-close"></span> Cancelar</a>
                                
                            </div>
                                

                        </div>
                    </div>
                    
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2"></div>
                </div>

		</div>
	</div>


@endsection