@extends('layouts.plantilla')
@section('contenidoGeneral')

	<div class="dashboard-wrapper">
		<div class="container-fluid dashboard-content ">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
					<div class="page-header">
						<h2 class="pageheader-title">Control COP - control de impresiones </h2> </div>
					</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
					<div class="page-header" align="right"> <img class="logo-img" src="{{ asset('assets/images/fergon.png') }}" alt="logo" height="45"> </div>
				</div>
			</div>			

			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="card">
						<div class="card-body">
							<div class="table-responsive">
								<table id="example" class="table table-striped table-bordered second" style="width:100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Usuario AD</th>
											<th>Equipo</th>
											<th>Paginas asignadas</th>
											<th>PC</th>
											<th>Acción</th>
										</tr>
									</thead>
									<tbody>
									@foreach ($asignar as $dato)
										<tr>
											<td>{{ $dato->id }}</td>
											<td>{{ $dato->UserPc }}</td>
											<td>{{ $dato->aEquipo }}</td>
											<td>{{ $dato->PagCout }}</td>
											<td>{{ $dato->users }}</td>
											<td>
												@if ($dato->PagCout != '' || $dato->PagCout != null)
													<a href="{{ route('asignar.edit',$dato->id)}}" class="btn btn-primary">Editar</a>	
													@else
													<button class="btn btn-default" disabled="">Editar</button>
												@endif
												
											</td>
										</tr>
									@endforeach												  
									</tbody>
										<tfoot>
											<tr>
												<th>ID</th>
												<th>Usuario AD</th>
												<th>Equipo</th>
												<th>Paginas asignadas</th>
												<th>PC</th>
												<th>Acción</th>
											</tr>
										</tfoot>
								</table>
							</div>
						</div>
					</div>
			   </div>
			</div>

		</div>
	</div>


@endsection