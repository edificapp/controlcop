@extends('layouts.plantilla')
@include('layouts.scandir')

<?php

$file = scandir::scan("./", "json");
foreach ($file as $value) {
	$elemento_encodificado = file_get_contents($value);
}
$resultado_uno = utf8_encode($elemento_encodificado);
$resultado_dos = json_decode($resultado_uno, true);
$resultado = $resultado_dos["Server"];

$cont_servidores = 0;
$cont_impresoras = 0;
$cont_impresiones = 0;
foreach ($file as $files) {  
	$data = file_get_contents($files);
	$variable = utf8_encode($data);
	$products = json_decode($variable, true);
	$datos = $products["Server"];
	if ($datos) {
		$cont_servidores++;
	}
	foreach ($datos as $data) {     
		if ($data['Impresora']) {
			$cont_impresoras++;
		}
		$cont_impresiones += $data['Impresion'];        
	}
}


?>

@section('contenidoGeneral')

	<div class="dashboard-wrapper">
		<div class="container-fluid dashboard-content ">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
					<div class="page-header">
						<h2 class="pageheader-title">Control COP - control de impresiones </h2> </div>
					</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
					<div class="page-header" align="right"> <img class="logo-img" src="{{ asset('assets/images/fergon.png') }}" alt="logo" height="45"> </div>
				</div>
			</div>

			<div class="row">
					<!-- data table  -->
					<!-- ============================================================== -->
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="card">
							<div class="card-header">
								<h5 class="mb-0">Usuarios COP</h5>
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<table id="example" class="table table-striped table-bordered second" style="width:100%">
										<thead>
											<tr>
												<th>Fecha</th>
												<th>ID</th>
												<th>Cant. de impresiones</th>
												<th>Impresora</th>
												<th>PC</th>
												<th>PC_red</th>
												<th>T. Archivo</th>
											</tr>
										</thead>
										<tbody>

										@foreach ($resultado as $dato)
											<tr>
												<td>{{ $dato['Fecha'] }}</td>
												<td>{{ $dato['IDeventoRecord'] }}</td>
												<td>{{ $dato['Impresion'] }}</td>
												<td>{{ $dato['Impresora'] }}</td>
												<td>{{ $dato['Pc'] }}</td>
												<td>{{ $dato['Pc_red'] }}</td>
												<td>{{ $dato['Tipo_archivo'] }}</td>
											</tr>
										@endforeach

										</tbody>
										<tfoot>
											<tr>
												<th>Fecha</th>
												<th>ID</th>
												<th>Cant. de impresiones</th>
												<th>Impresora</th>
												<th>PC</th>
												<th>PC_red</th>
												<th>T. Archivo</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- ============================================================== -->
					<!-- end data table  -->
					<!-- ============================================================== -->
				</div>
				</div>
			</div>
			

		</div>
	</div>


@endsection