@extends('layouts.plantilla_inicio')

@section('enlaces')

    <!doctype html>
    <html lang="es">
     
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Control COP</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 20px;
            padding-bottom: 20px;
        }
        </style>
    </head>
@endsection

@section('contenidoGeneral')

    <div class="dashboard-wrapper">
        <div class="container-fluid dashboard-content ">
            
                <!-- ============================================================== -->
                <!-- login page  -->
                <!-- ============================================================== -->
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xl-7">
                            <img class="img-fluid" src="{{ asset('assets/images/fondo.jpg') }}" width=""  height=""></a>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xl-5" style="background: #fff;">
                            <div class="splash-container">
                                <div class="card">
                                    <div class="card-header text-center"><a href="index.php"><img class="logo-img" src="{{ asset('assets/images/logo2.jpg') }}" alt="logo"></a><span class="splash-description">Solo personal autorizado.</span></div>
                                    <div class="card-body">
                                    <h3 style="font-size: 40px; text-align: center;color: #898989; font-family: arial;">INGRESO</h3>
                                        <form method="post" action="{{ route('login') }}">
                                        @csrf
                                            <div class="form-group">
                                                <label>{{ __('E-Mail Address') }}</label>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>{{ __('Password') }}</label>
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            
                                            <!--<input type="submit" class="btn btn-primary btn-lg btn-block" style="border-radius: 45px; margin-top: 30px;" name="loginSubmit" value="Entrar">-->
                                        
                                            <div class="form-group row">
                                                <div class="col-md-6 offset-md-4">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                        <label class="form-check-label" for="remember">
                                                            {{ __('Recordar') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Entrar') }}
                                                    </button>

                                                    @if (Route::has('password.request'))
                                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                                            {{ __('Olvido de contraseña?') }}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        @if (Route::has('login'))
                            <div class="top-right links">
                                @auth
                                    <a href="{{ url('/home') }}">Home</a>
                                @else
                                    <a href="{{ route('login') }}">Login</a>

                                    @if (Route::has('register'))
                                        <a href="{{ route('register') }}">Register</a>
                                    @endif
                                @endauth
                            </div>
                        @endif
                    </div>
                </div>
              
                <!-- ============================================================== -->
                <!-- end login page  -->
                <!-- ============================================================== -->         


        </div>
    </div>


@endsection
