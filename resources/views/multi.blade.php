@extends('layouts.plantilla')
@include('layouts.scandir')

@section('contenidoGeneral')

    <div class="dashboard-wrapper">
        <div class="container-fluid dashboard-content ">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                    <div class="page-header">
                        <h2 class="pageheader-title">Control COP - MULTIFUNCIONALES </h2> </div>
                    </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                    <div class="page-header" align="right"> <img class="logo-img" src="{{ asset('assets/images/fergon.png') }}" alt="logo" height="45"> </div>
                    
                </div>
            </div>           

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered second" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>MULTIFUNCIONALES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lista_de_impresoras as $data)

                                    <tr>
                                        <td><img class="logo-img" src="{{ asset('assets/images/02.png') }}" alt="logo" width="70" style="margin-right: 7px;"> {{$data}}</td>
                                                                              
                                    </tr>
                                    

                                    @endforeach

                                    </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>MULTIFUNCIONALES</th>
                                            </tr>
                                        </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
               </div>
            </div>

        </div>
    </div>


@endsection