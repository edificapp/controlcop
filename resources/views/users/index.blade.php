@extends('layouts.plantilla')
@section('contenidoGeneral')



    <div class="dashboard-wrapper">
        <div class="container-fluid dashboard-content ">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                    <div class="page-header">
                        <h2 class="pageheader-title">Gestion de administradores </h2> </div>
                    </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                    <div class="page-header" align="right"> <img class="logo-img" src="{{ asset('assets/images/fergon.png') }}" alt="logo" height="45"> </div>
                </div>
            </div>          

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered second" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($users as $user)
                                    <tr>      
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <!--a href="/users/{{ $user['id'] }}" class="btn btn-info btn-circle btn-sm" ><i class="fa fa-eye"></i></a>
                                            <a href="/users/{{ $user['id'] }}/edit" class="btn btn-warning btn-circle btn-sm"><i class="fa fa-edit"></i></a>
                                            <a href="#" data-toggle="modal" class="btn btn-danger btn-circle btn-sm" data-target="#deleteModal" data-userid="{{$user['id']}}"><i class="fas fa-trash-alt"></i></a-->
                                            
                                        </td>
                                    </tr>
                                    @endforeach                      
                                    </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Id</th>
                                                <th>Nombre</th>
                                                <th>Correo</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
               </div>
            </div>

        </div>
    </div>

@endsection
