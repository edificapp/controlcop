@extends('layouts.plantilla')
@include('layouts.scandir')

<?php

$file = scandir::scan("./", "json");

$cont_servidores = 0;
$cont_impresoras = 0;
$cont_impresiones = 0;
foreach ($file as $files) {  
    $data = file_get_contents($files);
    $variable = utf8_encode($data);
    $products = json_decode($variable, true);
    $datos = $products["Server"];
    if ($datos) {
        $cont_servidores++;
    }
    foreach ($datos as $data) {     
        if ($data['Impresora']) {
            $cont_impresoras++;
        }
        $cont_impresiones += $data['Impresion'];        
    }
}


?>

@section('contenidoGeneral')

    <div class="dashboard-wrapper">
        <div class="container-fluid dashboard-content ">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                    <div class="page-header">
                        <h2 class="pageheader-title">Control COP - control de impresiones </h2> </div>
                    </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                    <div class="page-header" align="right"> <img class="logo-img" src="{{ asset('assets/images/fergon.png') }}" alt="logo" height="45"> </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="card border-3 border-top border-top-primary">
                        <div class="card-body">
                            <h5 class="text-muted">Conectados</h5>
                            <div class="metric-value d-inline-block">
                                <h1 class="mb-1"><?php echo $cont_servidores; ?></h1> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="card border-3 border-top border-top-primary">
                        <div class="card-body">
                            <h5 class="text-muted">Registros</h5>
                            <div class="metric-value d-inline-block">
                                <h1 class="mb-1"><?php echo $cont_impresoras; ?></h1> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="card border-3 border-top border-top-primary">
                        <div class="card-body">
                            <h5 class="text-muted">Impresiones</h5>
                            <div class="metric-value d-inline-block">
                                <h1 class="mb-1"><?php echo $cont_impresiones; ?></h1> </div>
                        </div>
                    </div>
                </div>
            </div>

            

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered second" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>Usuario AD</th>
                                            <th>Equipo</th>
                                            <th>Cantidad restante</th>
                                            <th>Impresiones realizadas</th>
                                            <th>Paginas asignadas</th>
                                            <th>Limite max.</th>
                                            <th># Evento</th>
                                            <th>Opción</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($impre_usuario as $data)

                                    <tr>
                                        <td>{{$data->id}}</td>
                                        <td>{{$data->UserPc}}</td>
                                        <td>{{$data->EquipoPc}}</td>

                                        @foreach($asignarpaginas as $data2)
                                            @if( $data2->users  == $data->UserPc )

                                                <td>{{$data2->users}}</td>  

                                            @endif
                                        @endforeach
                                        
                                        @foreach($permi_usuario as $data3)
                                            @if(  $data3->id  == $data->id )

                                                @if( abs( $data3->Cout - $data->TotalCout ) == $data->TotalCout )
                                                    <td>0</td>                                              
                                                @else
                                                    <td>{{ abs( $data3->Cout - $data->TotalCout ) }}</td>
                                                @endif

                                            @endif
                                        @endforeach

                                        
                                        <td> {{$data->TotalCout}} </td>

                                        @foreach($asignarpaginas as $dataimpre)
                                            @if( $dataimpre->id  == $data->id )

                                                <td>{{$dataimpre->PagCout}}</td>  

                                            @endif
                                        @endforeach

                                        @foreach($permi_usuario as $data3)
                                            @if(  $data3->id  == $data->id )

                                                @if( $data3->Cout == null )
                                                    <td>0</td>  
                                                @else
                                                    <td>{{ $data3->Cout }}</td>
                                                @endif

                                            @endif
                                        @endforeach

                                        

                                        <td> {{$data->EventID}} </td>
                                                         
                                        @foreach($permi_usuario as $data3)
                                        
                                            @if(  $data3->id  == $data->id )

                                                @if( $data3->Cout == null )
                                                    <td><a class="btn btn-warning" href="<?php echo "recargar_usuario.php?id=" . $data->id ?>">Registrar Usuario</a></td>
                                                    <!--td><a class="btn btn-warning" href="<?php echo "recargar_usuario.php?id=" . $data->id ?>">Inactivo</a></td-->
                                                    <!--td><button class="btn btn-default" disabled="">Inactivo</button></td-->  
                                                    <td></td>  
                                                @elseif( $data3->Cout != null )
                                                    
                                                    @if ( $data->TotalCout >= $data3->Cout  ) 
                                                        <td><a class="btn btn-info" href="<?php echo " recargar_usuario.php?id=" . $data->id ?>">Activar</a></td>
                                                        <td>
                                                            @if( abs( $data3->Cout - $data->TotalCout ) == 0 )
                                                                <a class="btn btn-warning" href="{{ route('asignar.edit', $data->id)}}">Asignar paginas</a>
                                                            @endif
                                                            
                                                        </td>
                                                    @elseif (  $data->TotalCout < $data3->Cout )
                                                        <td><button class="btn btn-default" disabled="">Activo</button><td>
                                                        <button class="btn btn-default" disabled="">Imprimiendo</button>
                                                    @endif
                                                    
                                                @endif
                                            
                                            @endif
                                        @endforeach

                                        
                                    </tr>
                                    

                                    @endforeach

                                    </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>#ID</th>
                                                <th>Usuario AD</th>
                                                <th>Equipo</th>
                                                <th>Cantidad restante</th>
                                                <th>Impresiones realizadas</th>
                                                <th>Paginas asignadas</th>
                                                <th>Limite max.</th>
                                                <th># Evento</th>
                                                <th>Opción</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
               </div>
            </div>

        </div>
    </div>




@endsection