<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsignarpaginasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignarpaginas', function (Blueprint $table) {
            $table->id();
            $table->string('UserPc', 70)->nullable($value = true);
            $table->string('aEquipo', 70)->nullable($value = true);
            $table->integer('PagCout')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignarpaginas');
    }
}
