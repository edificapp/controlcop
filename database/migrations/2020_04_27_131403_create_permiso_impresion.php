<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermisoImpresion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permiso_impresion', function (Blueprint $table) {
            $table->id();
            $table->string('UserPc', 70)->nullable($value = true);
            $table->string('pEquipo', 70)->nullable($value = true);
            $table->integer('Cout')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permiso_impresion');
    }
}
