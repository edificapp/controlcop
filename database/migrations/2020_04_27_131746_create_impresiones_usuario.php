<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImpresionesUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('impresiones_usuario', function (Blueprint $table) {
            $table->id();
            $table->string('UserPc', 70)->nullable($value = true);
            $table->string('EquipoPc', 70)->nullable($value = true);
            $table->integer('TotalCout')->nullable($value = true);
            $table->string('Printer', 100)->nullable($value = true);
            $table->string('NArchivo', 200)->nullable($value = true);
            $table->string('EventID', 70)->nullable($value = true);
            $table->string('UserFecha', 70)->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('impresiones_usuario');
    }
}
